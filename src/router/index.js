import Vue from 'vue'
import Router from 'vue-router'

/*引入登陆页面*/
import Login from '@/components/Login'

/*引入接机页面*/
import Airport from '@/components/Airport'

/*引入送机页面*/
import SendMachine from '@/components/SendMachine'

/*引入接火车页面*/
import Train from '@/components/Train'

/*引入送火车页面*/
import GiveTrain from '@/components/GiveTrain'

/*引入设置页面*/
import MySet from '@/components/MySet'

/*引入历史订单页面*/
import Order from '@/components/Order'

/*引入结算页面*/
import Settlement from '@/components/Settlement'

/*引入结算页面*/
import Historical from '@/components/Historical'

/*引入子账号设置页面*/
import Children from '@/components/Children'



Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Login',
      component: Login
    },
    {
    	path: '/Airport',
    	name: 'Airport',
    	component: Airport
    },
    {
    	path: '/SendMachine',
    	name: 'SendMachine',
    	component: SendMachine
    },
    {
    	path: '/Train',
    	name: 'Train',
    	component: Train
    },
    {
    	path: '/GiveTrain',
    	name: 'GiveTrain',
    	component: GiveTrain
    },
    {
    	path: '/MySet',
    	name: 'MySet',
    	component: MySet
    },
    {
    	path: '/Order',
    	name: 'Order',
    	component: Order
    },
    {
    	path: '/Settlement',
    	name: 'Settlement',
    	component: Settlement
    },
    {
    	path: '/Historical',
    	name: 'Historical',
    	component: Historical
    },
    {
    	path: '/Children',
    	name: 'Children',
    	component: Children
    }
  ]
})

/*引入默认样式*/
import '../../static/public.css'
/*引入页面样式*/
import '../../static/index.css'
/*引入字体图标*/
import '../../static/font/iconfont.css'

/*引入时间样式*/
import '../../static/shijian.css'

/*引入时间js*/
import '../../static/js/jquer_shijian.js'